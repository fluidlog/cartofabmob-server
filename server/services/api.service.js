const ApiGatewayService = require('moleculer-web');
const { Routes: SparqlEndpointRoutes } = require('@semapps/sparql-endpoint');

module.exports = {
  mixins: [ApiGatewayService],
  dependencies: ['fuseki-admin'],
  settings: {
    routes: [
      {
        path: '/ontology',
        use: [
          ApiGatewayService.serveStatic('./public/ontology.ttl', {
            setHeaders: res => {
              res.setHeader('Access-Control-Allow-Origin', '*');
              res.setHeader('Content-Type', 'text/turtle; charset=utf-8');
            }
          })
        ]
      }
    ],
    cors: {
      origin: '*',
      methods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'HEAD', 'OPTIONS'],
      exposedHeaders: '*'
    }
  },
  methods: {
    authenticate(ctx, route, req, res) {
      return Promise.resolve(ctx);
    },
    authorize(ctx, route, req, res) {
      return Promise.resolve(ctx);
    }
  }
};
